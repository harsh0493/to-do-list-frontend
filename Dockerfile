# Stage 1
FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run prod_build

# Stage 2
FROM nginx:alpine
COPY --from=node /app/dist/to-do-list /usr/share/nginx/html