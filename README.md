# TO DO LIST FRONTEND

It is a frontend part of a web application created using Angular framework. This App interacts with the backend server by consuming REST APIs.

## Configuration
1. Go to src/environments/
2. Update API_ENDPOINT as per the environment (PROD, DEV) for which build is to be generated in the corresponding environment files.

## Installation & Run

**Install Dependencies**
```
npm install
```

**Run Tests**
```
ng test
```

**Run Application in Development Server**
```
ng serve
```

## Build

```
ng build
```

To generate build for production environment use --prod flag. Ex:
```
ng build --prod
```

## Run the App in Docker container

```
docker build -t to-do-list-frontend . && docker run -p 80:80 to-do-list-frontend
```