import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpResponse } from '@angular/common/http';
import {HttpTestingController} from '@angular/common/http/testing';

import { ToDoListService } from './to-do-list.service';
import { NotificationService } from './notification.service';
import { NotificationStub } from '../stubs/notification.stub';
import { environment } from '../../environments/environment';

describe('ToDoListService', () => {
  let service: ToDoListService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ToDoListService,
        {
          provide: NotificationService,
          useClass: NotificationStub
        }
      ]
    });
    service = TestBed.inject(ToDoListService);
    httpMock = TestBed.inject(HttpTestingController);
  });
  
  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('to be able to retrieve list from the GET API', () => {
    const dummyToDoList = [
      {
        ID: 1,
        Title: "Test Title 1",
        CreatedAt: "2021-04-12T12:34:08.123Z"
      },
      {
        ID: 2,
        Title: "Test Title 2",
        CreatedAt: "2021-04-12T13:24:08.123Z"
      }
    ];
    service.getToDoList().subscribe(response => {
      expect(response.length).toBe(2);
      expect(response).toEqual(dummyToDoList);
    });

    const request = httpMock.expectOne(`${environment.API_ENDPOINT}${service.apiURL}`);
    expect(request.request.method).toBe('GET');
    request.flush(dummyToDoList);
  });

  it('to be able to add new item in todo list using the POST API', () => {
    const dummyToDoItem = {
      ID: 3,
      Title: "Test Title 3",
      CreatedAt: "2021-04-12T14:44:08.123Z"
    };
    service.addTodo(dummyToDoItem).subscribe(response => {
      expect(response).toEqual(dummyToDoItem, 'should return the new item'),
      fail
    });

    // addTodo should have made one request to POST todo
    const request = httpMock.expectOne(`${environment.API_ENDPOINT}${service.apiURL}`);
    expect(request.request.method).toBe('POST');
    expect(request.request.body).toEqual(dummyToDoItem);

    // Expect server to return the new list item after POST
    const expectedResponse = new HttpResponse({ status: 200, statusText: 'Created', body: dummyToDoItem });
    request.event(expectedResponse);
  });
});
