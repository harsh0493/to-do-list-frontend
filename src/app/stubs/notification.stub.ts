export class NotificationStub {
    showSuccess(message: string, title: string): void { }
    showError(message: string, title: string): void { }
}