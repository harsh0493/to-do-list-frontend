import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { FormsModule } from "@angular/forms";

import { ToDoListComponent } from './to-do-list.component';
import { ToDoListService } from '../services/to-do-list.service';
import { NotificationService } from '../services/notification.service';
import { NotificationStub } from '../stubs/notification.stub';

describe('ToDoListComponent', () => {
  let component: ToDoListComponent;
  let fixture: ComponentFixture<ToDoListComponent>;
  let toDoListService: ToDoListService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule, FormsModule],
      providers: [
        ToDoListService,
        {
          provide: NotificationService,
          useClass: NotificationStub
        }
      ],
      declarations: [ ToDoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToDoListComponent);
    component = fixture.componentInstance;
    toDoListService = TestBed.get(ToDoListService);
    spyOn(toDoListService, 'getToDoList').and.callThrough();
    spyOn(toDoListService, 'addTodo').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title', () => {
    const titleElements = fixture.debugElement.queryAll(By.css('h2'));
    expect(titleElements.length).toBe(1);
    expect(titleElements[0].nativeElement.innerHTML).toBe('My To Do List');
  });

  describe('Getting the todo list', () => {
    it('should get the todo list from the service', () => {
      fixture.detectChanges();
      expect(toDoListService.getToDoList).toHaveBeenCalled();
    });
  });

  it('should not call addToDo api if title is empty', waitForAsync(() => {  
    component.title = '';
    let button = fixture.debugElement.nativeElement.querySelector('.addBtn');
    button.click();
  
    expect(toDoListService.addTodo).not.toHaveBeenCalled();
  }));

  it('should call addToDo api if title is not empty', waitForAsync(() => {  
    component.title = 'Test Title';
    let button = fixture.debugElement.nativeElement.querySelector('.addBtn');
    button.click();
  
    expect(toDoListService.addTodo).toHaveBeenCalled();
  }));
});
