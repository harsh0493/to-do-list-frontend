import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../services/notification.service';
import { ToDoListService } from '../services/to-do-list.service';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {

  toDoList = [];
  title = '';

  constructor(private notificationService: NotificationService,
    private toDoListService: ToDoListService) { }

  ngOnInit(): void {
    this.getToDoList(); // Get the todo list from server and render on view when the component is rendered
  }

  getToDoList() {
    const _this = this;

    // callback function to handle the response of getToDoList API
    const serviceCallback = function(response) {
      if (response) {
          _this.toDoList = response;
      } else {
        _this.notificationService.showError("Something went wrong. Unable to get the list from server.");
      }
    };

    this.toDoListService.getToDoList().subscribe(serviceCallback); // call the getToDoList API
  }

  /* Function to add an item in todo list and render it on UI again*/
  addTitle() {
    const _this = this;

    if (_this.title != '') {

      // API payload
      const data = {
        title: this.title
      };

      // callback function to handle the response of addToDoList API
      const serviceCallback = function(response) {
        _this.title = '';
        if (response) {
          _this.getToDoList(); // Get updated list from server and render on UI
          _this.notificationService.showSuccess("List updated successfully.");
        } else {
          _this.notificationService.showError("Something went wrong. Unable to addItem to the server.");
        }
      };

      this.toDoListService.addTodo(data).subscribe(serviceCallback); // call the addToDoList API
    }
  }

}
